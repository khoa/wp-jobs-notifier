<?php
namespace wp_jobs_notifier;

class emailer {

	public $email, $config, $db_host, $db_user, $db_pass, $db_database, $db_table;
	public $db;
	private $email_body = array();

	public $rss_feeds = array(
		'design',
		'development',
		'general',
		'migration',
		'performance',
		'plugin-development',
		'theme-customization',
		'writing'
	);


	function __construct( $rss_feeds = array() ) {
		$config = parse_ini_file( dirname(__FILE__) . '/db.ini', true);
		$this->config = $config;
		$this->email = $config['settings']['email'];
		$this->host = $config['database']['host'];
		$this->db_user = $config['database']['user'];
		$this->db_pass = $config['database']['password'];
		$this->db_database = $config['database']['database'];
		$this->db_table = $config['database']['table'];
		$this->db = new \mysqli( $this->db_host, $this->db_user, $this->db_pass, $this->db_database );
		if( mysqli_connect_errno() )
			die( 'Error: db connection failed:' . mysqli_connect_error() );
		if( !empty($rss_feeds) )
			$this->rss_feeds = $rss_feeds;

		if( !empty($config['settings']['timezone']) )
			date_default_timezone_set( $config['settings']['timezone'] );
	}


	public function get_feeds() {
		// https://gist.github.com/betweenbrain/5405671
		foreach( $this->rss_feeds as $rss_feed ) {

			$url = sprintf( "http://jobs.wordpress.net/job_category/%s/feed/", $rss_feed );

			$curl = curl_init();
			curl_setopt_array($curl, Array(
				CURLOPT_URL            => $url,
				CURLOPT_USERAGENT      => 'spider',
				CURLOPT_TIMEOUT        => 120,
				CURLOPT_CONNECTTIMEOUT => 30,
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_ENCODING       => 'UTF-8'
			));
			$data = curl_exec($curl);
			curl_close($curl);

			$rss = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
			#$rss = simplexml_load_file($url);

			foreach( $rss->channel->item as $item )
				$this->save_to_db( $item );
		}
		$this->send_email();


	}


	private function generate_email_text( $item ) {
		$meta_text = '';
		$description = $this->remove_null_string( $item->children('content', true) );
		$link_data = $this->scrape_data( $item->link );
		$footer_body = empty($this->config['settings']['signature']) ? '' : '%0A%0A%0A' . $this->config['settings']['signature'] . '%0A';
		$footer_body .= '%0A%0A' . $item->link . '%0A';

		if( isset($link_data['apply_text']) && $link_data['apply_text'] == 'email' )
			$apply_text = sprintf( ' - <a href="%s?subject=%s&body=%s">%s</a>', $link_data['apply_href'], $item->title, $footer_body, $link_data['apply_text'] );
		elseif( isset($link_data['apply_text']) )
			$apply_text = sprintf( ' - <a href="%s">%s</a>', $link_data['apply_href'], $link_data['apply_text'] );
		else
			$apply_text = '';

		$fields = array( 'company', 'jobtype', 'location', 'budget' );
		foreach( $fields as $field ) {
			if( isset($link_data[$field]) )
				$meta_text .= '<b>' . ucfirst($field) . ':</b> ' . $link_data[$field] . "<br />\n";
		}

		$email_body = sprintf( "
				<h3><a href='%s'>%s</a>%s</h3>
				<p>
					%s
					<b>Posted</b>: %s<br />
				</p>
				<p>%s</p>
			", $item->link, $item->title, $apply_text, $meta_text, date('M d, g:i a', strtotime($item->pubDate)), $description );

		return $email_body;
	}

	private function send_email() {
		if ( empty( $this->email_body ) )
			return;

		$email_subject = 'Wordpress jobs - ' . date( 'M d, g:i a' );
		$email_body = implode( '<hr />', $this->email_body );

		$headers[]  = 'MIME-Version: 1.0';
		$headers[]  = 'Content-type: text/html; charset=iso-8859-1';
		$headers[]  = 'From: WP Jobs <' . $this->config['smtp']['user'] . '>';
		$headers[]  = 'Reply-To: ' . $this->config['smtp']['user'];
		$headers[]  = 'X-Mailer: PHP/' . phpversion();
		mail( $this->email, $email_subject, $email_body, implode( "\r\n", $headers ) );
	}


	private function save_to_db( $item ) {
		$creator = $item->children('dc', TRUE);
		$created = date('Y-m-d H:i:s', strtotime($item->pubDate));
		$short_description = $this->remove_null_string( $item->description );
		$long_description = $this->remove_null_string( $item->children('content', true) );
		$parts = parse_url($item->guid);
		parse_str($parts['query'], $query);
		$guid = $query['p'];

		$sql = sprintf( "SELECT id FROM %s WHERE guid = %d", $this->db_table, $guid );
		$result = $this->db->query( $sql );

		if( $result->num_rows == 0) {
			$sql = sprintf( "INSERT INTO %s (guid, created, title, author, description, description_short, url) VALUES ( '%d', '%s', '%s', '%s', '%s', '%s', '%s' )", $this->db_table, $guid, $created, $item->title, $creator, mysqli_real_escape_string($this->db, $long_description), mysqli_real_escape_string($this->db, $short_description), $item->link );
			$result = $this->db->query( $sql );

			$this->email_body[] = $this->generate_email_text( $item );
		}

	}

	public function scrape_data( $url ) {

		$data = array();
		$html = file_get_contents( $url );
		$dom_doc = new \DOMDocument();
		libxml_use_internal_errors(TRUE); //disable libxml errors

		if( !empty($html) ){
			$dom_doc->loadHTML($html);
			libxml_clear_errors();
			$dom_xpath = new \DOMXPath($dom_doc);

			$fields = array( 'company', 'jobtype', 'location', 'budget' );
			foreach( $fields as $field ) {
				$row = $dom_xpath->query('//*[@class="job-' . $field . '"]/dd');
				if( $row->length > 0 ){
					foreach( $row as $row )
						$data[$field] = $row->nodeValue;
				}
			}

			$apply = $dom_xpath->query('//*[@class="job-howtoapply"]/dd/a');
			if($apply->length > 0){
				foreach( $apply as $apply ) {
					$data['apply_text'] = $apply->nodeValue;
					$data['apply_href'] = $apply->getAttribute('href');
				}
			}
		}
		return $data;
	}


	public function install_db() {

		if( isset($_GET['action']) && $_GET['action'] == 'truncate' ) {
			$query = $this->db->query( "TRUNCATE TABLE $this->db_table" );
			if( $query === TRUE )
				echo "<h3>Notifications table TRUNCATE OK</h3>";
			else
				echo "<h3>Notifications table NOT truncated</h3>";
			exit;
		} elseif( isset($_GET['action']) && $_GET['action'] == 'drop' ) {
			$query = $this->db->query( "DROP TABLE IF EXISTS $this->db_table" );
			if ( $query === true ) {
				echo "<h3>Notifications table DROPPED OK</h3>";
			} else {
				echo "<h3>Notifications table NOT dropped</h3>";
			}
		}

		$sql = "CREATE TABLE IF NOT EXISTS $this->db_table (
                id INT(11) NOT NULL AUTO_INCREMENT,
                guid INT(11) NOT NULL,
                created DATETIME NOT NULL,
                title VARCHAR(255) NOT NULL,
                author VARCHAR(255) NOT NULL,
                description TEXT NOT NULL,
                description_short TEXT NOT NULL,
                url VARCHAR(255) NOT NULL,
                PRIMARY KEY (id)
        )";
		$query = $this->db->query($sql);
		if( $query === TRUE )
			echo "<h3>Notifications table created OK</h3>";
		else
			echo "<h3>Notifications table NOT created</h3>";
	}


	public function remove_null_string( $text ) {
		return str_replace( '<span class="oe_displaynone">null</span>', '', $text );
	}
}